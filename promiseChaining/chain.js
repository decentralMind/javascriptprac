//https://codeburst.io/playing-with-javascript-promises-a-comprehensive-approach-25ab752c78c3

// let promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         reject('done', 1000)
//     });
// });

// promise
// .then(console.log)
// .catch(console.log);

// let myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('done', 1000);
//     })
// });

// myPromise.then((x) =>{
//     console.log('first x', x) 
//     return x; //x is done here
// })
// .then((x) => {
//     var y = x; // y is done here
//     console.log('y', y);
// })
// .then((x) => {
//     console.log('final x', x) // x is undefined here because nothing is returned from previous promise.
//     return x;
// });

//Promise inside promise
// promise1 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('promise1', 1000);
//     });
// });

// promise2 = new Promise((resolve, reject) => {
//     setTimeout(() =>{
//         resolve('promise2', 1000)
//     });
// });

// promise1.then((first)=>{
//     return promise2.then((second) => {
//         console.log('first', first);
//         console.log('second', second);
//     });
// });


//In real life analogy, If you want to be trustable to people, always promise/oath to deliver something to someone if it's within
//your scope. If you are unable to fullfill it give a reason to the person and have a talk about it instead of staying silence.

//Always returned a promise when it's chained or nested.
let promise1 = new Promise(function(resolve, reject){
    setTimeout(function(){
        resolve('foo', 1000);
    });
});

promise1.then(function(success){
    console.log(success);
    return promise2;
})
.then(function(data){
    console.log(data);
    return promise3;
})
.then(function(data){
    console.log(data);
});

let promise2 = new Promise(function(resolve, reject){
    setTimeout(function(){
        resolve(2, 2000);
    });
});

let promise3 = new Promise(function(resolve, reject){
    setTimeout(function(){
        resolve(3, 5000);
    })
});


